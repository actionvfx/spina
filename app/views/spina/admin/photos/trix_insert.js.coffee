if '#<%= params[:object_id] %>' == '#slider-first'
    $(document).trigger('first-photo-selected', '<%= @photo.file.url %>')
else if '#<%= params[:object_id] %>' == '#slider-second'
    $(document).trigger('second-photo-selected', '<%= @photo.file.url %>')
else
    $('#<%= params[:object_id] %>').trigger('photo-insert', '<%= @photo.file.url %>')

$.hideModal()
