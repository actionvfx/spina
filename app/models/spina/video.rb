module Spina
  class Video < ApplicationRecord
    if CarrierWave::Uploader::Base.storage == CarrierWave::Storage::File
      mount_uploader :file, VideoUploaderDevelopment
    else
      mount_uploader :file, VideoUploader
    end

    has_many :page_parts, as: :page_partable
    has_many :layout_parts, as: :layout_partable
    has_many :structure_parts, as: :structure_partable

    def name
      file.file.filename
    end

    def content
      self
    end

    def self.order_by_ids(ids)
      sql = sanitize_sql_for_assignment({id: ids})
      order("CASE WHEN #{sql} THEN 0 ELSE 1 END")
    end
  end
end
