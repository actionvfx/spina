module Spina
  class Ability
    include CanCan::Ability

    def initialize(user)
      if user.admin?
        return can :manage, :all
      end

      if user.site_editor?
        can :manage, Page, locked: false
        can :manage, Account
      end

      can :read, Page
      can :manage, Photo
      can :manage, Attachment
      can :manage, Video
    end
  end
end
