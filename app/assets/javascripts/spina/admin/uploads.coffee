 # Upload photo
$.fn.uploadPhoto = ->
  $(this).fileupload
    dataType: "script"
    singleFileUploads: true
    dropZone: $(this).find('.photo-field')
    # maxNumberOfFiles: 1
    add: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        $(this).find('.customfile').addClass('loading')
        data.submit()
      else
        alert("#{file.name} is geen gif-, jpeg- of png-bestand")
    done: (e, data) ->
      $(this).find('.customfile').removeClass('loading')

# Upload video
$.fn.uploadVideo = ->
  $(this).fileupload
    dataType: "script"
    singleFileUploads: true
    add: (e, data) ->
      uploadVideoIfValid(data, $(this))
    done: (e, data) ->
      $(this).find('.customfile').removeClass('loading')

# Upload video directly to S3
$.fn.directlyUploadVideo = ->
  $(this).fileupload
     type: "POST"
     url: $(this).attr('action')
     dataType: 'xml'
     add: (e, data) ->
       uploadVideoIfValid(data, $(this))
     success: (data) ->
       key = $(data).find('Key').text()
       $.ajax({
          type: 'POST',
          url: '/admin/videos',
          dataType: 'script'
          data: {key: key}
          success: (data) ->
            console.log('success', data)
        })
      done: (e, data) ->
        $(this).find('.customfile').removeClass('loading')

uploadVideoIfValid = (data, $el) ->
  types = /(\.|\/)(mp4|mpe?g|mov)$/i
  file = data.files[0]
  if types.test(file.type) || types.test(file.name)
    if file.size <= (1024*1024*8)
      $el.find('.customfile').addClass('loading')
      data.submit()
    else
      alert("File is #{parseInt(file.size/(1024*1024))}MB. It needs to be smaller than 8MB")
  else
    alert("#{file.name} needs to be either: mp4, mpeg, or mov")

# Upload document
$.fn.uploadDocument = ->
  $(this).fileupload
    dataType: "script"
    singleFileUploads: true
    dropZone: $(this).find('.attachment-field')
    maxNumberOfFiles: 1
    add: (e, data) ->
      file = data.files[0]
      if file.size < 100000000
        $(this).find('.customfile').addClass('loading')
        data.submit()
      else
        alert("File is #{parseInt(file.size/(1024*1024))}MB. It needs to be smaller than 100MB")
    done: (e, data) ->
      $(this).find('.customfile').removeClass('loading')
