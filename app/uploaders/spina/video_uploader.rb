module Spina
  class VideoUploader < DefaultStoreUploader
    include CarrierWaveDirect::Uploader

    def store_dir
      "#{mounted_as}/#{model.class.to_s.underscore}"
    end
  end
end
