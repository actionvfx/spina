module Spina
  module Admin
    class VideosController < AdminController
      before_action :set_uploader, only: [:index, :video_select]
      layout "spina/admin/media_library"

      def index
        add_breadcrumb I18n.t('spina.website.videos'), spina.admin_videos_path
        @videos = Video.order('created_at DESC').page(params[:page])
      end

      def create
        @video = Video.create(safe_params)
        respond_to do |format|
          format.js
        end
      end

      def destroy
        @video = Video.find(params[:id])
        @video.destroy
        redirect_to spina.admin_videos_url
      end

      def video_select
        @selected_video_id = Video.find_by(id: params[:selected_video_id]).try(:id)
        @hidden_field_id = params[:hidden_field_id]
        @videos = Video.order_by_ids(@selected_video_id).order('created_at DESC').page(params[:page])
      end

      def insert_video
        @video = Video.find(params[:video_id]) if params[:video_id].present?
      end

      private

      def safe_params
        if using_file_storage?
          params.require(:video).permit([:file])
        else
          params.permit([:key])
        end
      end

      def set_uploader
        @uploader = Video.new.file
        @uploader.use_action_status = true
        @uploader.success_action_status = "201"
      end
    end
  end
end
