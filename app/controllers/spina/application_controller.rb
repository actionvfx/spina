module Spina
  class ApplicationController < ActionController::Base

    protect_from_forgery with: :exception

    include ApplicationHelper

    private

    def current_ability
      @current_ability ||= Ability.new(current_spina_user)
    end

    def current_spina_user
      @current_spina_user ||= ::Spina::User.where(id: session[:user_id]).first if session[:user_id]
    end
    helper_method :current_spina_user

    def using_file_storage?
      CarrierWave::Uploader::Base.storage == CarrierWave::Storage::File
    end
    helper_method :using_file_storage?
  end
end
