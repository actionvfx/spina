class CreateSpinaMultiLines < ActiveRecord::Migration
  def change
    create_table :spina_multi_lines do |t|
      t.text :content

      t.timestamps null: false
    end
  end
end
