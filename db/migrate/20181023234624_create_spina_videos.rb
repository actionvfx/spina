class CreateSpinaVideos < ActiveRecord::Migration
  def change
    create_table :spina_videos do |t|
      t.string :file

      t.timestamps null: false
    end
  end
end
