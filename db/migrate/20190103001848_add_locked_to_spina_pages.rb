class AddLockedToSpinaPages < ActiveRecord::Migration
  def change
    add_column :spina_pages, :locked, :boolean, default: false
  end
end
