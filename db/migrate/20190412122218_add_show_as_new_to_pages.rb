class AddShowAsNewToPages < ActiveRecord::Migration
  def change
    add_column :spina_pages, :show_as_new, :boolean
  end
end
