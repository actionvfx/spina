class AddCustomSlugToSpinaPages < ActiveRecord::Migration
  def change
    add_column :spina_pages, :custom_slug, :string
  end
end
